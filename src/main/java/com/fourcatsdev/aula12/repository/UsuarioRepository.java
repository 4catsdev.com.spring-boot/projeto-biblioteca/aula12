package com.fourcatsdev.aula12.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula12.modelo.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	Usuario findByLogin(String login);
}
